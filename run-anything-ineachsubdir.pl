#!/usr/bin/perl

if ($#ARGV < 1) {
	print "Use: $0 program-to-run list-of-subdirs\n";
	exit;
}
$program=shift(@ARGV);
$listfile=shift(@ARGV);

open (LIST, "<$listfile");
while (<LIST>) {
	chomp;
	$subdir_array{$_}=$_;
}

opendir (DIR, '.');
@filenames = readdir(DIR);
foreach $filename (sort @filenames) {
	#if  (-d $filename && $filename=~/^[0-9][a-zA-Z]/) {
	if  (-d $filename && $subdir_array{$filename}) {
		chdir $filename;
		print "directory: $filename,,\n";
		system "pwd";
		print "$program\n";
#		system "./$program"; #or give the path of wherever the script, program resides
		system "$program";
		chdir "..";
	}
	#chdir "..";
}

