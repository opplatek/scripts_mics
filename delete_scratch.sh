if [ -z ${SCRATCH} ] # If variable is UNSET or HAS zero length ;if [ -z ${SCRATCH+x} ] # If variable is UNSET but CAN be zero length
then 
	echo "SCRATCH is unset or is empty"
else 
	echo "SCRATCH is set to '$SCRATCH', deleting"
	rm -r $SCRATCH
fi
