#!/bin/bash
#
# Convert miRBase (U) to a reference (T) and select and organism
#

wget ftp://mirbase.org/pub/mirbase/21/hairpin.fa.gz
zcat hairpin.fa.gz | perl -pe '/^>/ ? print "\n" : chomp' | tail -n +2 | sed '/^[^>]/ y/uU/tT/' > hairpin.oneline.fasta
grep -A1 "^>hsa" hairpin.oneline.fasta | sed '/^--$/d' > hsa_hairpin.fasta