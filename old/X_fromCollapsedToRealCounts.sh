# Multiply number of lines by the number of collapsed reads
# Input is BAM file from the mapping and collapsed reads from fastx_collapser tool
# Takes line by line from the BAM (SAM) file, parses them to get the number of reads and multiplies
#	the input BAM (SAM) file

for i in *bam
do
$SAMTOOLS_BIN/samtools view $i > ${i%.*}.tmpCounts.sam # Create SAM file

counter=1 # Set counter

cat ${i%.*}.tmpCounts.sam | awk 'BEGIN {FS="-"} {print $2}' | awk '{print $1}' > blb # Take SAM file
	# and extract number of occurences of each collapsed read

numberOfLines=$(wc -l blb | awk '{print $1}') # Count number of of lines = number of unique collapsed read names

while [ $counter -le $numberOfLines ] # While number of lines is lower or equal to the counter value continue
	# in calculation
do

multiply=$(awk -v var="$counter" 'NR==var {print $0}' blb) # Save number of of occurences for specific line 

awk -v var="$counter" -v var2="$multiply" 'NR==var {for(i=0;i<var2;i++)print}' ${i%.*}.tmpCounts.sam >> ${i%.*}.tmpCounts2.sam # Take number of occurences
	# and line number and multiple specific line with number of occurences = creates the "true" .sam output file with correct number of lines for 
	# each mapping

$SAMTOOLS_BIN/samtools view -b ${i%.*}.tmpCounts2.sam > ${i%.*}.tmpCounts2.bam # SAM to BAM conversion

counter=$(($counter+1)) # Increase the counter by one and go back to the beginning
			# Increase the counter by one for up to number of lines in blb = number
			# of unique reads
done

mv ${i%.*}.tmpCounts2.bam $i # Move temporary BAM file to the original BAM file

rm ${i%.*}.tmpCounts.sam # Remove temporary SAM file
rm ${i%.*}.tmpCounts2.sam # Remove another temporary SAM file
rm blb # Remove temporary file

done

