#!/bin/bash
#
# Convert RNA (U) fasta to DNA (T) fasta
#

if [[ $# -eq 0 ]] ; then
    echo 'Please, enter exactly one argument with RNA (U) fasta to convert.'
    exit 0
fi

if [[ $# -eq 1 ]] ; then
	echo 'Converting ${1} to DNA (U->T)'
	sed '/^[^>]/ y/uU/tT/' $1 > ${1%.*}.dna.fa
else
	echo 'Please enter EXACTLY one argument - one RNA fasta (U) to be converted to DNA (T).'
fi
