#!/bin/sh
#
# Make sed command faster - splits input file and launches all sed commands together
#

INFILE=$1
THREADS=6 # How many times should be the input split?
SEDSCRIPT=/home/jan/Data/databases/repeatmasker/error.sed # Each sed command on a separate line; for example line contains only "s/chr1/1/g" without quotes to replace chr1 for 1

filename=$(basename $INFILE)
extension="${filename##*.}"

mkdir -p ~/BigData/tmp/sed_chr
cd ~/BigData/tmp/sed_chr

# Check and remember if input if gzipped
gzip="no"
if [ $extension == "gz" ]
then 
	unpigz -c -p $THREADS $INFILE > ${INFILE%.$extension}
	INFILE=${INFILE%.$extension}
	extension="${INFILE##*.}"
	gzip="yes"
fi

SPLITLIMIT=`wc -l $INFILE | awk -v var="$THREADS" '{print $1 / var}'` # How many lines should be in a split chunk?

rm x_*
split -d -l $SPLITLIMIT $INFILE x_

echo "Started renaming"

for chunk in $(ls x_??)
do
  echo $chunk
  sed -f $SEDSCRIPT $chunk > $chunk.out &
done

wait 

cat x_??.out >> ${INFILE%$extension}noErr.$extension

rm -f x_??
rm -f x_??.out

pigz -p $THREADS ${INFILE%$extension}noErr.$extension

if [ $gzip == "yes" ]
then 
	rm $INFILE
fi
