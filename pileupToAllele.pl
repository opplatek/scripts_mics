##################
#!/usr/bin/perl 
##########################################################################################
# Script to extract alleles from Pileup file.                                       #
# It is Based on samtools.pl:        #
##########################################################################################

use strict;
#use warnings;

&usage if (@ARGV < 1);

my $command = shift(@ARGV);
my %func = (pileup2allele=>\&pileup2allele);

die("Unknown command \"$command\".\n") if (!defined($func{$command}));
&{$func{$command}};
exit(0);

# pileup2allele

sub pileup2allele {
  die(qq/
Usage:   extract.allele.4m.strand.pileup.pl pileup2allele <input.chr.raw-pileup>
/) if (@ARGV == 0 && -t STDIN);

my @qual;
my @line;
my $phred=10; # Phred quality threshold
my $sanger=33; # Sanger quality=33 # solexa/illumina=64 

# Reading input pileup 
  while (<>) {
    @line = split;
      if ($line[2] eq "*"){next;} # skipping indel row #
      $line[4] =~ s/[+-][0-9]+[atgcrykmswbdhvnATGCRYKMSWBDHVNF]+//g; # Removes unwanted extra-indels notation from read base lines
      $line[4] =~ s/\^.//g; # Removes unwanted symbols
      $line[4] =~ s/[0-9\@\'\&\!\?\_\$\*\:\;\F\"\+\#\-\=\%\)\(\/\~\}\[]+//g; # Removes unwanted symbols 
      $line[4] =~ s/\./$line[2]/g; # Substituting . with reference base 
      $line[4] =~ s/\,/lc($line[2])/ge; # Substituting , with lower case reference base 

#      $line[4] = uc($line[4]); # Converting to UPPER case

      # Store quality scores as an array
      @qual = split(//, $line[9]);
      # Transform quality values from ASCII into Sanger format
      for( my $i = 0; $i < scalar @qual; $i++ ){
       $qual[$i] = ord($qual[$i]) - $sanger ;
           }


    my $char;
    my %chars=();
    my $chars;
    my @letters = split(//, $line[4]);
    my $top=0;
    print "$line[1]\t$line[4]\t"; # print nucleotide position and the Sequence from the READS

    # Quality comparison 
    foreach $char(@letters) { if ($qual[$top] >= $phred){ $chars{ $char }++; } $top++; }
    my @keys = sort { $chars{$b} <=> $chars{$a} } keys %chars; # sorting higher to lower value
    foreach my $key(@keys){ print $key, " ", $chars{$key},"\t";}
    print "\n";

  } # End of while

 } # End of pileup

##################

sub usage {
my $str ='Sample Input Pileup Data
10      56256   A       A       75      0       21      16      .,+4gcgc,,,,,,..,...,,  13?5??>5<9=71570';
  die(qq/
Usage:   extract.allele.4m.strand.pileup.pl pileup2allele <input.chr.raw-pileup>  \n
$str \n/);
}

# ----------------------------------------------------  

#2. If pipe the pileup through samtools output
##samtools pileup -cf reference.fasta  Input.bam | ./extract.allele.4m.strand.pileup.pl pileup2allele >OUTPUT.TXT

# ----------------------------------------------------  

## End of the Script ######
