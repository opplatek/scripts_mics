### Python script for replacing words by pattern in external file
### Could be used for example to rename genes from one annotation to another one
### The input is the pattern where in first column are names to be replaced and in the second column
###	separated by tab are names for what the first column will be replaced
### Another required input is the file where you want to replace the names
### Output is then new file with replaced names in the same folder as the input file
###
### Launch the script: python substituter.py patterns.txt toreplace.txt

import sys
import re

pattern_name = sys.argv[1]

target_name = sys.argv[2]

def pattern_load(pattern_name):
	pattern_file = open(pattern_name)
	pattern = {}
	for tline in pattern_file:
		line = tline[0:-1]
		if line != 'Input Type':
			splitter = line.split()
			if len(splitter) == 2:
				pattern[splitter[0]] = splitter[1]
	pattern_file.close()
	return pattern

def target_load_sub(target_name, pattern):
	target_file = open(target_name)
	target = []
	newtarget = open("new_"+target_name, 'w')
	for tline in target_file:
		line = tline[0:-1]
		linelist = []
		#line2 = line.replace(",", " ").replace(";", " ")
		#splitter = line2.split()
		#newl = line
		#test = {}
		newl = line
		for item in pattern:
			line2 = newl.replace(",", " ").replace(";", " ")
	        	splitter = line2.split()
	        	#newl = line
			if item in splitter:
				newl = re.sub(r'\b'+item+r'\b', pattern[item], newl)
				
				#newl = newl.replace("\b"+item+"\b",pattern[item])
		#print newl	
		newtarget.write(newl + '\n')
	newtarget.close()
	target_file.close()
	return target


patterns = pattern_load(pattern_name)
#print patterns	
targets = target_load_sub(target_name, patterns)

