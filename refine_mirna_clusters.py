#!/usr/bin/env python

#this script is made for refining the clusters after merging them with TSS
#for previous steps please see /jan/home/Projects/mirna_clusters/clusters_mirgen/miRGen_clus_to_csv.sh
#
#input is the files from mentioned .sh and the output is refined mirna clusters separated by "--" sign
#
#there are some post processing steps in miRGen_clust_to_csv.sh
#

import re, sys, os, os.path

pattern = '.bed'#define pattern for which files the script should be executed


files = filter(lambda l : l.endswith(pattern) , os.listdir(sys.argv[1]))
for f in files:
	count = 0;
	current = ''
	previous = ''

	file = open(os.path.join(sys.argv[1], f[:-len(pattern)] + '.ref_clus'), 'w')
	for line in open(os.path.join(sys.argv[1], f), 'r').readlines():
		splitted = re.split('\s', line)
		id = splitted[3]

		if id[0:5] == 'mirna':
			current = re.findall(r'mirna_\d+_', id)[0]

			if(current != previous): 
				file.write('--\n')
				count = count+1
		
			flag = 'f' if splitted[5] == '+' else 'r'
			file.write(line[:-1] + '\t' + flag +str(count) + '\n')
			previous = current

		else:
			if previous[0:5] == 'mirna':
				previous = ''
				file.write('--\n')
			file.write(line)

	file.close()
