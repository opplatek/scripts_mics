#!/usr/bin/env perl
#
# Get Ensembl cannonical transcripts for each gene
#
# https://groups.google.com/forum/#!topic/biomart-users/skO4zgqzGBA
#
# Get MySQL database name here http://ftp.ensembl.org/pub/release-87/mysql/homo_sapiens_core_87_38/

use strict;
use warnings;
 
use Bio::EnsEMBL::Registry;
my $reg = "Bio::EnsEMBL::Registry";
 
$reg->load_registry_from_db(
   -host => 'ensembldb.ensembl.org',
   -user => 'anonymous',
   -dbname  => 'homo_sapiens_core_87_38'
);
 
open(OUTFILE, ">human_canonical_transcripts_v87.fa");
 
my $gene_adaptor = $reg->get_adaptor('human', 'core', 'gene');
my @genes = @{$gene_adaptor->fetch_all};
 
while(my $gene = shift @genes) {
  print
    (OUTFILE
        $gene->canonical_transcript->stable_id, ".", $gene->canonical_transcript->version, "\n");
}