#!/usr/bin/env python
#
# Very simple script to extract just GO names from a file with description as well as GO groups
# GO groups in the input file are written several times on each line in the format
#			"cytolysis in other organism [GO:0051715]; pathogenesis [GO:0009405]"
# and the script extracts for each line just ['GO:0051715', 'GO:0009405']
#

import re 

with open('INPUT_TO_REPLACE') as f:
    for line in f:
        x = re.findall(r'\[GO:(.*?)\]', line) # Extract just strings that are between [GO: and ]
	print x
