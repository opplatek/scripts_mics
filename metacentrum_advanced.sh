#!/bin/bash

trap 'clean_scratch' TERM EXIT

DATADIR="/storage/brno3-cerit/home/$LOGNAME"

#Vytvoreni adresaru pro ukladani vysledku
mkdir $SCRATCHDIR/FastQC || rm -rf $SCRATCHDIR/FastQC/*
mkdir $SCRATCHDIR/Minion_res || rm -rf $SCRATCHDIR/Minion_res/*
mkdir $SCRATCHDIR/fasta_trimmed || rm -rf $SCRATCHDIR/fasta_trimmed/*

#Pridani potrebnych modulu
module add fastQC-0.10.1
module add python27-modules-gcc

export PATH=$PATH:$DATADIR

#Minion adapter search
ADAPTERS=/storage/brno3-cerit/home/barborah/adapters_merge.txt
OUTPUT_FOLDER=$SCRATCHDIR/Minion_res
OUTPUT_CUTADAPT=$SCRATCHDIR/fasta_trimmed


#########################################################################################################
#
# samotne zpracovani jedne sekvence, v $1 je jeji nazev,
# napr. ./15ff3ee8-45db-4af5-b424-563517eb56b2/121129_UNC16-SN851_0193_BC12YPACXX_GGCTAC_L002_1.fastq
# v $name je pak jen 121129_UNC16-SN851_0193_BC12YPACXX_GGCTAC_L002_1.fastq
#
#########################################################################################################
function process_one()
{
        name=${1##*/}
        # Predicts the adapters
        minion search-adapter -i $SCRATCH/$name -show 3 -write-fasta $OUTPUT_FOLDER/${name%.*}.minion.fasta 
        # Compares predicted adapters with database
        swan -r $ADAPTERS -q $OUTPUT_FOLDER/${name%.*}.minion.fasta > $OUTPUT_FOLDER/${name%.*}.minion.compare 
        SEARCH=`head -1 $OUTPUT_FOLDER/${name%.*}.minion.compare | sed -e 's/^[ ]*\([ACTG]*\).*/\1/'`
        cutadapt -b $SEARCH  -O 5 -o $OUTPUT_CUTADAPT/${name%.*}.trimmed.fastq $SCRATCH/$name 
        fastqc -o $SCRATCHDIR/FastQC $SCRATCH/$name
}

############################################################################
#
# resynchronizace vysledku, rsync dokopiruje automaticky ty, co chybi
# pousti se do pozadi, aby mohl mezitim bezet vypocet
# predpokladam, ze kopirovani 1 souboru ze serveru a vysledku na server 
# je rychlejsi nez samotny vypocet, zejmena to cutadapt
#
############################################################################
function reupload()
{
        rsync -a $SCRATCHDIR/Minion_res -e ssh tcga.ceitec.muni.cz:/mnt/nfs3sec/data/Results &
        rsync -a $SCRATCHDIR/FastQC -e ssh tcga.ceitec.muni.cz:/mnt/nfs3sec/data/Results &
        rsync -a $SCRATCHDIR/fasta_trimmed -e ssh tcga.ceitec.muni.cz:/mnt/nfs3sec/data/Results &
}

cd $SCRATCHDIR;

run='';

########################################################################################################################
#
# hlavni smycka pres vsechny soubory na tcga serveru
# v $i jsou nazvy jako: ./15ff3ee8-45db-4af5-b424-563517eb56b2/121129_UNC16-SN851_0193_BC12YPACXX_GGCTAC_L002_1.fastq
#
########################################################################################################################
for i in `ssh tcga.ceitec.muni.cz "cd /mnt/nfs3sec/data/cghub_untar/; find -name *.fastq"`; do
        scp -q tcga.ceitec.muni.cz:/mnt/nfs3sec/data/cghub_untar/$i $SCRATCH &
        reupload
        if [ x$run != x ]; then
                process_one $run;
        fi
        run=$i;
        wait;
done

####################################################################################
#
# protoze se vypocet pousti vzdy o jedno pozdeji, aby se mezitim stahl dalsi soubor
# je nutne dodelat pak ten posledni, co zbyl
#
####################################################################################
if [ x$run != x ]; then
        process_one $run;
fi

# upload it
reupload
wait;
