#!/usr/bin/perl -anF\t
### \t in previous line specifies the cell separator
# Use like this perl extractLinesByList.pl tmp.list < mergedTableAll.csv
# Use like this perl extractLinesByList.pl listOfWords < FromWhereToExtract
# Extracts all rows with specific word(s) from a list - quick extraction of a large amount of selected
#	rows

use strict;
our %names;

BEGIN {
    while (<ARGV>) {
        chomp;
        $names{$_} = 1;
    }
}

print if $names{$F[1]};
