#!/usr/bin/perl

# cluster orfs based on their self-match table
#i.e. a table in format: orf(i)<tab>orf(j)<tab>rest_of_lines;
while (<>) {
	chop;
	($id,$orf)= split ;
    	#($id,$orf,$rest) = split ;
    	$self_array{$id}=$id; #20080318
    	#$self_array{$orf}=$orf;
	$self_pw{$_}=$_;
    	if ($orf ne $id) { 
		if ($orf_array{$orf}) {#orf already belongs to a clus
	    		$clusterid=$orf_array{$orf};
	    		@clusterlist=split("\t",$id_hits{$clusterid});
	    		$flag=0;
	    		foreach $elem (@clusterlist) {
				if ($elem eq $id) {
		    			$flag=1;
				}
	    		}
	    		if (!$flag) {#$id not in the cluster yet which orf belongs to
				$id_hits{$clusterid}.="$id\t"; #CLUSTER ASSIGNMENT
				$orf_array{$id}=$clusterid; #link id to a cluster
	    		}
		} elsif ($orf_array{$id}) {# $id has already a cluster,orf not yet
		# orf appears for the first time
			$clusterid=$orf_array{$id}; 
			@clusterlist=split("\t",$id_hits{$clusterid});
			$flag=0;
			foreach $elem (@clusterlist) {
		    		if ($elem eq $orf) {
					$flag=1;
		    		}
			}
			if (!$flag) { #if $orf is not in the cluster yet to which orf belongs
		    		$id_hits{$clusterid}.="$orf\t"; #CLUSTER ASSIGNMENT
		    		$orf_array{$orf}=$clusterid; #this is the right way to link orf
			}
	    	} else {#both id and orf are new
			$id_hits{$id}="$id\t$orf\t"; #NEW CLUSTER (new 2007.03.05)
			$orf_array{$orf}=$id; #link orf to id, cluster just created
			$orf_array{$id}=$id;  #link id to id (itself), cluster just created
	    	}
	} #if ($orf ne $id)	 
} 

#compare each cluster with one another:
foreach $clusterid1 (keys %id_hits) {
    foreach $clusterid2 (keys %id_hits) {
	if ($clusterid2 gt $clusterid1) {
	    if (shared_cluster($clusterid2,$clusterid1)) {
#		print "shared $clusterid1,$clusterid2\n";
		push @SHARED, "$clusterid1\t$clusterid2";
	    }
	}
    }
}

#process @SHARED, by adding one $clusterid's values to the other, and deleting the superfluous one
foreach $pair (@SHARED) {
    ($clusterid1,$clusterid2)=split("\t",$pair);
    if ($id_hits{$clusterid1}) { #either clusterid1 or clusterid2 might be missing
	unify($clusterid1,$clusterid2);
	delete $id_hits{$clusterid2}; #delete the second clusterid cluster
    } else {
	unify($clusterid2,$clusterid1);
	delete $id_hits{$clusterid1}; #delete the second clusterid cluster
    }
}

#by now, we should have only one cluster for each element
foreach $clusterid (keys %id_hits) {
	@clusterlist=split("\t",$id_hits{$clusterid});
	$clustered_hash{$clusterid}=$clusterid; #mark as clustered
    	#print "$clusterid\t"; ### why was this line necessary? #20070923
	foreach $elem (@clusterlist) {
		$clustered_hash{$elem}=$elem; #mark as clustered
		print "$elem\t";
    	}
    	print "\n";
}

#print pairs that don't have both elems clustered already
foreach $pair (keys %self_pw) {
	($id,$orf)=split(/\t/,$pair);
	if (!$clustered_hash{$id} || !$clustered_hash{$orf}) {
		if ($orf ne $id) {		
			print "$id\t$orf\n";
    		}
	}
}

#find singular elements:
foreach $id (keys %self_array) {
	if (!$orf_array{$id}) {
		print "$id\n";
	}
}

exit;

sub shared_cluster {
    my ($id1, $id2) = @_;
    $fl=0;
    @list1=split("\t",$id_hits{$id1});
    @list2=split("\t",$id_hits{$id2});
    unshift(@list1,$id1);
    unshift(@list2,$id2);
    foreach $elem1 (@list1) {
	foreach $elem2 (@list2) {
	    if ($elem1 eq $elem2) {
		$fl=1;
	    }
	}
    }
    return $fl;
}

sub unify {
    	my ($id1, $id2) = @_;
    	@list1=split("\t",$id_hits{$id1});
    	@list2=split("\t",$id_hits{$id2}); 
    	unshift(@list2,$id2);
	%list1_array={};
    	foreach $elem1 (@list1) {
		$list1_array{$elem1}=$elem1; 
    	}
    	foreach $elem2 (@list2) {
		if ($list1_array{$elem2}) {
		#elem2 already in @list1, do nothing
		} else {
	    		$id_hits{$id1}.="$elem2\t";
		} 
    	}
}
