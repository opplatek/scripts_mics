###################################################################################################
###  Reference used
# http://www.ncbi.nlm.nih.gov/nuccore/NC_021490.2

###  Tool used
# https://github.com/alimanfoo/pysamstats
###################################################################################################

###
# Specify input directory
INPUT_DIR=/home/jan/Projects/linda_syfilis/Klinicke_vzorky_syfilis/SS14-like

# Specify reference fasta
REF_FASTA=/home/jan/Projects/linda_syfilis/Klinicke_vzorky_syfilis/NC_021490.2.fasta


### Command used
# For all bam files in the folder write nucleotide distribution for each position

for inputFile in $INPUT_DIR/*rmdup.real.bam
	do
		pysamstats -f $REF_FASTA --type variation_strand $inputFile > ${inputFile%.*}.baseDist.txt
	done
