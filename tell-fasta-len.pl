#!/usr/bin/perl 

#tell the length of entries in a multiple FASTA format sequence file, listing taxids too
#if ($#ARGV <0 ) {
#  print "Usage: $0 fastafile\n";
#  exit(0);
#}

#$fasta=shift(@ARGV);
#open (FASTA, "<$fasta");

#while (<FASTA>) {
while (<>) {
  chomp;
  #is current line beginning of a new record?
#  if (/^>(\S+) *(\S+)*/) {
#    if (/^>([\S\-]+)/) {#specifically for intact.fasta format
    if (/^>(\S+)/) {
    #was there an old record to print out?
    if ($name ne "") {
      finish_entry($name, $seq);
    }
    #initialise new record
    $name=$1; $taxid=$2;
    $seq="";
  } else {
    #read some sequence and concatenate with stuff already read
    $seq=$seq.$_;
  }
}
#last record:
finish_entry($name, $seq);

sub finish_entry {
	my ($name,$seq)= @_;
#  $len=length($seq);
	$dashless="";
	for ($i=0;$i<length($seq);$i++) {
		if (substr($seq,$i,1) =~ /[a-zA-Z]/ ) {
			$dashless.=substr($seq,$i,1);
		}
	}
	$len=length($dashless);	
  	print "$name\t$len\n";
}
