#!/usr/bin/perl

#blastn output processing
########################################################################
#@2006 Copyright of Hedi Hegyi                                         # 
#This note is not to be removed from this script                       #
#The script can be used only with the explicit permission of the author#
########################################################################
#maximum number of consecutive matches

print "query\tsprot\tseq_st\tseq_end\tquery_st\tquery_end\tevalue\tpercid\tquerylength\thitlength\tscore\n";
while (<>) {
    if (/^Query\= +(\S+) *(.+)*/) {
    #if (/^Query\= +(\S+)/) {
	$qctr++;
	if ($Query) {
	    finish_query($Query,$Queryrec,$Queryrest);
	}
	$Query=$1;
	$Queryrest=$2;
	$Queryrec="";
        #print STDERR "back in main loop: $qctr-th Query: $Query; Queryrest=$Queryrest\n";
    } else {
	$Queryrec=$Queryrec.$_;
    }
}
if ($Query) {
    finish_query($Query,$Queryrec,$Queryrest);
}
sub finish_query {
    my ($Q,$Qrec,$Qrest) = @_;
    $querylen=0;
    $querylenflag=1;
    my $name="";
    my $rec="";
    my $Sdes="";
    @querylist=split("\n",$Qrec);
    $linenum=0;
    while (@querylist) {
	$line=shift(@querylist);
#	if ($line=~ /\((\S+) letters\)/) {#changed to
	if ($line=~ /^Length=([0-9]+)/ && $querylenflag) { #this line, 2012.10.19.
                $querylen=$1;
		$querylenflag=0; #2012.10.19. #so that first Length=...is observed here
        }
	$linenum++;
	#is current line beginning of a new record?
#	if ($line=~ /^>(\S+) *(.*)$/) {
       if ($line=~ /^> (\S+) *(.*)$/) {#inserted a space into reg exp 2012.10.19.
		#was there an old entry to process?
		if ($name) {	# 
			finish_entry($Q,$name,$Sdes,$rec,$Qrest);
		}			# 
	    	$name  = $1;
	    	$Sdes = $2;
	    	$rec="";
	} elsif ( $line=~ /\*\*\*\*\* No hits found \*\*\*\*\*/) {
	   	last; #??? what is the correct statemnt?
	} else {
	    	$rec=$rec.$line."\n";
	}
    }
    if ($name) {
	finish_entry($Q,$name,$Sdes,$rec,$Qrest);
    }
}

close BLASTPFILE;
sub finish_entry { 
  my ($Q,$entry,$Sdes,$stuff,$Qrest) = @_;
  my $score=0;
  my $len=0;
  my $evalue=0;
  my $Lengthflag=0;
  my $fulldes=$Sdes;
  $alignments_start=0;
  @stufflist=split("\n",$stuff);
  while (!$len ) {
    $line=shift(@stufflist);
    #print "line=$line\n";
    if ($line=~ /^Length=([0-9]+)/ ) {#changed reg exp, 2012.10.19.
    	$Lengthflag=1;
      $len=$1;
    } elsif (!$Lengthflag) {
    	$fulldes=$fulldes.$line;
  	}
  }
  while (@stufflist) {
    $line=shift(@stufflist);
    if ($line=~ / +Score = +(\S+) +bits +\((.+)\), +Expect\S* = (\S+)/) {#if new alignment starts
      $alignments_start=1;
      if ($score ) {
	  	finish_aln($Q,$entry,$fulldes,$aln,$len,$score,$evalue,$Qrest);
      }				# 
      $score=$2; $evalue=$3;
      $aln="";
    }#if
    elsif ($alignments_start) {
      $aln.="$line\n";
    }
  }#while
  finish_aln($Q,$entry,$fulldes,$aln,$len,$score,$evalue,$Qrest);
}

sub finish_aln {
	my ($query,$entry,$Sdes,$aln,$len,$score,$evalue,$Qrest) = @_;
  	@alnlist=split("\n",$aln);
	$cons_string="";
	while (@alnlist ) {
		$line=shift(@alnlist);
#		if ($line=~ / +Score .+ Expect = +(\S+)/) {
		if ($line=~ / Expect = +(\S+)/) { #2012.10.19.
			$evalue=$1;
			print STDERR "evalue=$evalue!!\n";
    		}
    		if ($line=~ / +Identities = +(\d+)\/(\d+) +\((\S+)\%\)/ ) {
			$ident=$1; $ali_len=$2; $percid=$3;
#			print STDERR "percid=$percid!!\n";
      			$first_alignment=1; 
      			$query_line=1;
      			$consensus_line=0;
    		}
    		else {
      			if ($line=~ /^Query +([0-9]+)( +)(\S+) +([0-9]+)/ && $query_line) {#if query seq, : removed, 2012.10.19.
				$query_e=$4;
				$spaces=$2;
				if ($first_alignment) {
	  				$query_s=$1;
					$startstring="Query  $query_s".$spaces; # : replaced with ' ',2012.10.19.
					$precons=length($startstring);
				}
				$$consensus_lindc=1; #'consensus' line coming
				$query_line=0;
				next; #go to next line in "while (@alnlist)" 
      			}
      			if ($consensus_line) {
				if ($line =~ /^[ ]{$precons}(.+)$/) {
					$cons_string.=$1; #add consensus to cons string	
				}
				$consensus_line=0;
				$sbjct_line=1;
				next;
      			}
      			if ($line=~ /^Sbjct +([0-9]+) +(\S+) +([0-9]+)/ ) {#if entry sequence,: removed, 2012.10.19.
				$seq_e=$3;
				if ($first_alignment) {
	  				$seq_s=$1;
	  				$first_alignment=0;
				}
				$sbjct_line=0;
				$query_line=1;
      			} 
		}        
	} #while 
	if ($evalue =~ /^e/) {
      		$evalue='1'.$evalue;
  	}
	#$Sdes1=substr($Sdes,0,30);
   	#print "cons_string=$cons_string\n";
	$matches=0; $maxmatches=0;
	$cons_substr="";
	$i=0;
	#for ($i=0;$i<=length($cons_string);$i++) {
	while ( $i<length($cons_string) ) {
		$ch3=substr($cons_string,$i,1);
		if ($ch3=~/[A-Z]/) {
			$matches++;
			$cons_substr.=$ch3;
		}
		elsif ($ch3=~/[ \+]/) {
			if ($matches>$maxmatches) {
				$maxmatches=$matches;
			} 
			$matches=0;
			#print "cons_substr=$cons_substr\n";
			#print "maxmatches=$maxmatches\n";
			$cons_substr="";
		}
		$i++;
	}
	if ($matches>$maxmatches) {
		$maxmatches=$matches;
        }
	#print "$query\t$entry\t$seq_s\t$seq_e\t$query_s\t$query_e\t$evalue\t$percid\t$querylen\t$len\t$maxmatches\n";
	print "$query\t$entry\t$seq_s\t$seq_e\t$query_s\t$query_e\t$evalue\t$percid\t$querylen\t$len\t$score\t$Sdes\n";
	#print "$query\t$entry\t$seq_s\t$seq_e\t$query_s\t$query_e\t$evalue\t$percid\t$querylen\t$len\t$score\t$Qrest\n";
}
