#
# It seems some versions of Picard put different header to the output files and then multiQC doesn't know which one it is
# This fixes the issue
# Probably would be better to change it directly in multiQC
#
 
for i in *.markDups_metrics.txt
do
sed -i 's/MarkDuplicates/picard.sam.markduplicates.MarkDuplicates/g' $i
done

for i in *.output.RNA_Metrics.txt
do
sed -i 's/CollectRnaSeqMetrics/picard.analysis.CollectRnaSeqMetrics/g' $i
done
