### Adds 5 N's to each FASTA file
### Not optimal, but works
### http://stackoverflow.com/questions/26027344/fastest-way-to-add-ns-and-concatenate-millions-of-sequences-in-a-fasta

import sys
from Bio import SeqIO
from Bio.Seq import Seq
in_file = open(sys.argv[1],'r')
sequences = SeqIO.parse(in_file, "fasta")
#concat=Seq("")
for record in sequences:
    record.seq=("N" * 5) + record.seq + ("N" * 5)
    #concat+=record.seq
    print ">" + record.id	
    print record.seq
#print record.seq
