#!/bin/bash
#using HTSeq-count to count features from aligned .sam file
#setting -s no -a 5 is was created for RNA-Seq analysis
#-s no means you set unstranded analysis (you do not know from
#   which strand the transcript is. This is usuall setting and shoud
#   not be changed unless you know what you are doing!
#-a 5 means it will not count read with mapping quality lower then 
#     5. The default is 0, but 5 is usually recommended to skip very low
#     quality mapping reads.
#Other setting are in default mode which is recommende for RNA-Seq analysis. 
#You might want to add "--mode intersection-nonempty" to get more counts
#but first see explanation at http://www-huber.embl.de/users/anders/HTSeq/doc/count.html

#not exporting this variable might cause problems with sorting by name using sort command
export LC_ALL=POSIX

inputFile=C3PEGACXX_1560_13s008049-1-1_Zuna_lane513s008049_sequence.sam
#inputFile=C3PEGACXX_1587_13s008050-1-1_Zuna_lane513s008050_sequence.sam
#inputFile=C3PEGACXX_1624_13s008051-1-1_Zuna_lane513s008051_sequence.sam
#inputFile=C3PEGACXX_1646_13s008052-1-1_Zuna_lane513s008052_sequence.sam
#inputFile=C3PEGACXX_1674_13s008053-1-1_Zuna_lane613s008053_sequence.sam
##inputFile=C3PEGACXX_1678_13s008054-1-1_Zuna_lane613s008054_sequence.sam
#inputFile=C3PEGACXX_1786_13s008055-1-1_Zuna_lane613s008055_sequence.sam
#inputFile=C3PEGACXX_1789_13s008056-1-1_Zuna_lane613s008056_sequence.sam

inputPath=/g/solexa/RunVol1/Runs/oppelt/zuna
outputPath=/g/solexa/RunVol1/Runs/oppelt/zuna/res

#It is recommended to use ENSEMBLE annotation in most cases which could be 
#downloaded from http://www.ensembl.org/info/data/ftp/index.html for most
#of typical organisms. Look for "Gene sets" column and .gtf file.
#Another annotation could be obtained for example from UCSC http://hgdownload.cse.ucsc.edu/downloads.html
#but both of these annotations might and probably will contain DIFFERENT annotated
#genes!!! so these annotation should not be "compared" since they have different
#things inside.
#Annotation for htseq-count needs to be modified if using ENSEMBLE annotation.
#1. Change names of chromosomes in annotation to match name from reference genome -
#   most often names in reference genome are "chr1" but in annotation file are only
#   "1". To change this do "sed 's/^/chr/g' input.gtf > modified.gtf which will 
#   add "chr" at the begining of each line in annotation file.
#2. Also it is pretty common to have "chrMT" in annotation file and "chrM" in ref-
#   erence genome (mitochondrial chromosome). To change this do "sed -i 's/chrMT/chrM' modified.gtf"
#   which will change the names right in the annotation file. 
#3. Important is to delete gene annotation header from gene annotation file. Usually
#   it has something like "#gene_version..." at first few lines of the annotation file. 
#	These lines need to be deleted before processing by HTSeq-count otherwise it gives
#	you error message saying "Needs more than one value to process or something".
pathToAnnotation=/g/solexa/RunVol1/Runs/oppelt/genome
annotationName=Homo_sapiens.GRCh37.75_ht.gtf

#sort sam file by name
#you could use also "samtools sort -n" but have to apply this on .bam file and not on .sam
#file and then convert it back to .sam. It means one additional step. Results are the same,
#though.

file=$inputPath/$inputFile
annotation=$pathToAnnotation/$annotationName

#this command sorts .sam file by name and output this to a new file keeping the original one
#New version of HTSeq-count is reported to handle unsorted .sam but I did not observe this yet

sort -s -k 1,1 $file > $outputPath/${inputFile%.*}.name_sort.sam

#This launches HTSeq-counting from script created during HTSeq-count installation. It might be 
#better to use "proper" HTSeq-count installed program probably in your /bin directory or in 
#/bin directory of HTSeq-count folder.i
#NOTE: HTSeq-count have problems with .sam file that were produced with alignment allowing more
#than one mapping or fusion genes or uncommon pairing. This would probably give you following
#error message - "MRNM == '*' although flag bit &0x0008 clearedMRNM == '*' although flag bit &0x0008 cleared"
#and the program crashes. Such .sam file were reported to be produced by GSNAP in default setting (allowing
#100 mappings and +1 suboptimal level and MapSplice allowing non-cannonical splice sites and fusion non-cannonical
#splice sites. While using STAR or RUM, everythin went ok. It might be fixed in new versions of HTSeq-count 
#(test on latest version on 3/12/2014) but so far it is not. It is reported bug and can be "fixed" by removing
#lines with string \t\*\t. To do this you can perform "sed '/\t\*\t/d input.sam > string.deleted.sam' which will 
#delete all lines with \t\*\t pattern and
#write it to a new file. Then you can launch counting again on a new .sam file but you will most probably receive
#a lot of warning messages saying there is a missing mate. It might help to convert modified .sam to .bam 
#"samtools view -Sb string.deleted.sam > string.deleted.bam, sort .bam by name 
#"samtools sort -n string.deleted.bam string.deleted.name_sort", then fix mate information 
#"samtools fixmate string.deleted.name_sort.bam string.deleted.name_sort.matefix.bam", then back to .sam
#"samtools view -h string.deleted.name_sort.matefix.bam > string.deleted.name_sort.matefix.sam" and then 
#launching HTSeq-count on new .sam file again. It probably will give you warning messages again. If it crashes, 
#use the "original" string delete name sorted .sam file

#consider changing -a 10 (default) to get only reads with higher mapping quality
python2.7 -m HTSeq.scripts.count -s no -a 5 $outputPath/${inputFile%.*}.name_sort.sam $annotation > $outputPath/${inputFile%.*}.htseq.count

#uncomment this if you do not want to keep .sam file sorted by name after the analysis. 
#suitable if you know you will not use HTSeq-count in some other setting.
#rm $outputPath/${inputFile%.*}.name_sort.sam 
