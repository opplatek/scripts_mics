md5 : 339d82627a41645b32c79a042f5a3d1c
name : motifBS_pl_339d
version : ""
language : C
language_id : C
installed : 0
date_compiled : Mon Jul 20 16:26:17 2015
inline_version : 0.78
ILSM : %
    module : Inline::C
    suffix : so
    type : compiled
Config : %
    apiversion : ?
    archname : x86_64-linux-gnu-thread-multi
    cc : cc
    ccflags : -D_REENTRANT -D_GNU_SOURCE -DDEBIAN -fstack-protector -fno-strict-aliasing -pipe -I/usr/local/include -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64
    ld : cc
    osname : linux
    osvers : 3.2.0-58-generic
    so : so
    version : 5.18.2
