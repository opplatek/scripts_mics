md5 : 168b5562b2d3d613c6ee4dee4c45c915
name : Bio::DB::IndexedBase_168b
version : ""
language : C
language_id : C
installed : 0
date_compiled : Wed Feb 15 12:55:54 2017
inline_version : 0.80
ILSM : %
    module : Inline::C
    suffix : so
    type : compiled
Config : %
    apiversion : ?
    archname : x86_64-linux-gnu-thread-multi
    cc : cc
    ccflags : -D_REENTRANT -D_GNU_SOURCE -DDEBIAN -fstack-protector -fno-strict-aliasing -pipe -I/usr/local/include -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64
    ld : cc
    osname : linux
    osvers : 3.13.0-79-generic
    so : so
    version : 5.18.2
