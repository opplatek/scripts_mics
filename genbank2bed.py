#!/usr/bin/env python
# encoding: utf-8

#
# Works only for single chromosome "bacteria" genomes
#
#

"""
bed_from_genbank.py input.gbk output.bed

grab the gene records from a genbank file (edit for other record types).

- requires:  biopython

"""

from Bio import SeqIO

import pdb
import sys

def main():
    outf = open(sys.argv[2], 'w')
#    header = """track name=vitVinGenes description="V. vinifera cpdna genes" itemRgb=On\n"""
#    outf.write(header)
    for record in SeqIO.parse(open(sys.argv[1], "rU"), "genbank") :
#	print(record.id)
        for feature in record.features:
            if feature.type == 'gene':
                start = feature.location.start.position
                stop = feature.location.end.position
                try:
                    name = feature.qualifiers['gene'][0]
                except:
                    # some features only have a locus tag
                    name = feature.qualifiers['locus_tag'][0]
                if feature.strand < 0:
                    strand = "-"
                else:
                    strand = "+"
                bed_line = "{0}\t{1}\t{2}\t{3}\t1000\t{4}\n".format(record.id, start, stop, name, strand)
                outf.write(bed_line)
    outf.close()


if __name__ == '__main__':
    main()
