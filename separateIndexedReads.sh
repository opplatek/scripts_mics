#!/bin/bash
#
# Find indexed fastq reads and output them - need to have all reads with the same length!
# If a read has index in first n bases and last n bases it will read the fastq, grep all reads with both indexes and output them
# Might be slow and indexes MUST NOT be in the name of read or in quality line
#

### Variables - change them according to current analysis
INDEX1="AAAAAA" # Index 1 - from left
INDEX2="TTTTT" # Index 2 - from right

INPUT=test.fq # Input file
OUTPUT=test.$INDEX1.$INDEX2.fq # Output file

LEN=120 # Length of the sequences

LEN_LEFT=30 # Number of bases from the start where to look for Index 1
LEN_RIGHT=$LEN_LEFT # Number of bases from the end where to look for Index 2 - default is the same as for Index 1

### Script body

BACK=$[$LEN-$LEN_RIGHT] # Store back of the sequence where to look for Index 2

if [ -f $OUTPUT ]; then # If output file already exists give warning and exit - this is because we append the output to one file
    echo -e "Output file" $OUTPUT "already exists! Is this on purpose?\n"
	read -n1 -r -p "Write 'y' to continue...else abort the mission!" key
	if [ "$key" = 'y' ]; then
		echo -e "\n 'y' written, continue the analysis!"
	else
		echo -e "\n 'y' NOT written, abort the analysis!"
		exit
	fi
fi 

echo "Processing input file" $INPUT "with indexes" $INDEX1 "for left index and" $INDEX2 "for right index. Will output file" $OUTPUT.

a=$RANDOM # Generate random number for tmp results and not to ovewrite other results

cat $INPUT | cut -c 1-$LEN_LEFT  | grep -n $INDEX1 | awk -F ":" '{print $1}' | sort | uniq > ${a}lines1 # Cut first 30 characters and grep Index 1 and output matched line numbers
cat $INPUT | cut -c $BACK-$LEN | grep -n $INDEX2 | awk -F ":" '{print $1}' | sort | uniq > ${a}lines2 # Cut last 30 characters and grep Index 2 and output matched line numbers

cat ${a}lines1 ${a}lines2 | sort | uniq -d > ${a}lines # Output lines numbers with both Indexes

cat ${a}lines | while read line # Read lines numbers one by one
do 
	start=$[$line-1] # Store fastq read - start
	end=$[$line+2] # Store fastq read - end 
	sed -n ${start},${end}p $INPUT >> $OUTPUT # write result to output
done

rm ${a}lines1 ${a}lines2 ${a}lines # Clean
