#!/usr/bin/perl

if ($#ARGV < 3) {
	print "Use: $0 program-to-run[e.g. insert-smalltable-intobig.pl] input1_ext input2_ext output_ext\n";
	exit;
}
$programcall=shift(@ARGV);
$extin1=shift(@ARGV);
$extin2=shift(@ARGV);
#$param=shift(@ARGV);
$extout=shift(@ARGV);

opendir (SUBDIR, '.');
@filenames = readdir(SUBDIR);
foreach $infile (sort @filenames) {
        if  ($infile=~/^(\S+)\.$extin1$/) {
		$infile2="$1.".$extin2;
		$outfile="$1.".$extout;
		if (-s $outfile) { #if outfile already exists and size > 0
			#do nothing
		} else {
			print  "$programcall $infile $infile2  > $outfile\n";
			system "$programcall $infile $infile2  > $outfile";
		}
	}
}

