#!/bin/bash
#
#####same as cutadapt.sh only 5' end primer trimming is commented out
#
#This script removes 3' and 5' end adapters/linkers from reads using cutadapt tool and 
#writes out statistics
#
#cutadapt has to be in PATH directory if not following lines export it's 
#location do PATH
#
#export PATH to cutadapt
#first specify PATH to cutadapt bin if it is not in PATH already
#PATH=$PATH:/home/embl/oppelt/tools/cutadapt-1.3/bin
#export PATH
#
#specify name of the dataset if there are more than one in the input folder
DATASET=SRP002861
#specify input and output directory
PATH_TO_FILES=/home/jan/Projects/mirna_clusters/clusters_datasets/preprocessed/$DATASET
OUTPUT_DIRECTORY=/home/jan/Projects/mirna_clusters/clusters_datasets/preprocessed/$DATASET
#specify suffix of files you want to process when using ls $PATH_TO_FILES/*.$SUFFIX > reads.lst
#if not, comment this line out
SUFFIX=.fastq
#suffix for files for 5' adapter trimming
SUFFIX2=.ad3trim.fq
#set file format of the data
FILE_FORMAT=fastq
#adapter on 3 prime end
ADAPTER3=TGGAATTCTCGGGTGCCAAGGAACTCCAGTCAC
#adapter on 5 prime end, if any
ADAPTER5=CGACAGGTTCAGAGTTCTACAGTCCGACGATC
##
#allowed error rate ERROR_RATE*100=percentage of error rate, 0.12 is standard
#for 3' end trimming
ERROR_RATE=0.11
#the same for 5' end trimming
ERROR_RATE2=0.11
#minimal overlap between adapter and read for 3' end trimming
#in short reads lets say 6 in longer it might be higher
MIN_OVERLAP=6
#the same for 5' end trimming
MIN_OVERLAP2=6
#discard reads shorter than DISC_SHORT
#depends on what you want to do. If it is only counting of TAG it can be set as low as 50
#acceptable length of RNA-seq reads is around 76 in a normal experiment so it should not 
#be much higher than this
DISC_SHORT=10
#in older version cutadapt wasn't able to correctly recognize correct phred coding
#default phred is 33, in case of 64 add --quality-base=64
#
#if we want to keep untrimmed reads in a separate file add this to the code
# --untrimmed-output=${line}.noad3trim

#create read list from PATH_TO_FILES
#or just give it a list of files in reads.lst line by line
ls $PATH_TO_FILES/*$SUFFIX > reads.lst

#this part generates unique number for reads.lst so it is not overwritten later
#NUMBER_LIST=$RANDOM
#cat reads.lst > $NUMBER_LIST.reads.lst

#rm reads.lst

#execute program and trim 3' end adapters
#while read list; do
#line="${list%.*}"
#cutadapt -f $FILE_FORMAT -a $ADAPTER3 -e $ERROR_RATE -O $MIN_OVERLAP -m $DISC_SHORT -o ${line}.ad3trim.fq --info-file=${line}.ad3info --too-short-output=${line}.ad3short $list 
#done < $NUMBER_LIST.reads.lst

#execute program and trim 5' end adapters
ls $PATH_TO_FILES/*$SUFFIX2 > reads.lst

#this part generates unique number for reads.lst so it is not overwritten later
NUMBER_LIST2=$RANDOM
cat reads.lst > $NUMBER_LIST2.reads.lst

rm reads.lst

while read list; do
line="${list%.*}"
cutadapt -f $FILE_FORMAT -g $ADAPTER5 -e $ERROR_RATE2 -O $MIN_OVERLAP2 -m $DISC_SHORT -o ${line}.ad5trim.fq --info-file=${line}.ad5info --too-short-output=${line}.ad5short $list 
done < $NUMBER_LIST2.reads.lst

#create output directory
#mkdir $OUTPUT_DIRECTORY

#mv $PATH_TO_FILES/*.ad5short $OUTPUT_DIRECTORY/
#mv $PATH_TO_FILES/*.ad5info $OUTPUT_DIRECTORY/
#mv $PATH_TO_FILES/*.ad5trim.fq $OUTPUT_DIRECTORY/
#mv $PATH_TO_FILES/*.ad3short $OUTPUT_DIRECTORY/
#mv $PATH_TO_FILES/*.ad3info $OUTPUT_DIRECTORY/
#mv $PATH_TO_FILES/*.ad3trim.fq $OUTPUT_DIRECTORY/

#remove reads list if desired otherwise comment out following line
#rm $NUMBER_LIST.reads.lst
rm $NUMBER_LIST2.reads.lst

