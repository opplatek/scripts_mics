#!/bin/bash
#
#This script trims low quality 3' end from reads in input files and 
#discards reads shorter than specified length after trimming
#
#fastx-toolkit has to be in $PATH directory
#
#export PATH to Fastx-toolkit
#first specify PATH to Fastx-toolkit bin if it is not in PATH already
#PATH=$PATH:/home/oppelt/tools/installation/bin
#export PATH
#
#specify input and output directory
PATH_TO_FILES=/home/jan/Projects/mirna_clusters/clusters_datasets/preprocessed
OUTPUT_DIRECTORY=/home/jan/Projects/mirna_clusters/clusters_datasets/preprocessed
#specify dataset
DATASET=SRPCalogero

#specify suffix of files you want to process when using ls $PATH_TO_FILES/*.$SUFFIX > reads.lst
#if not, comment this line out
SUFFIX=.trimad.fq

#if quality is in phred +64 delete -Q 33
#default is phred +33
#percentage of each reads that has to have quality at least PHRED_QUAL_FIL
PERC_OF_READ=95
#set the phred quality score for filtering
PHRED_QUAL_FIL=20

#create output directory if doesn't exist
#mkdir $OUTPUT_DIRECTORY

#create read list from PATH_TO_FILES
#or just give it a list of files in reads.lst line by line
ls $PATH_TO_FILES/$DATASET/*$SUFFIX > reads.lst


NUMBER_LIST=$RANDOM
cat reads.lst > $NUMBER_LIST.reads.lst

rm reads.lst

while read line; do
line="${list%.*}"
fastq_quality_filter -Q 33 -v -q $PHRED_QUAL_FIL -p $PERC_OF_READ -i $list -o ${line}.qf.fq
done < $NUMBER_LIST.reads.lst

rm $NUMBER_LIST.reads.lst
