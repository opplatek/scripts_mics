for i in *.txt
do
awk '{$1=""; print}' $i > tmp
mv tmp $i
sed -i 's/" "/\t/g' $i
sed -i 's/"  "/\t/g' $i
sed -i 's/"   "/\t/g' $i
sed -i 's/"    "/\t/g' $i
sed -i 's/"     "/\t/g' $i
sed -i 's/"//g' $i
sed -i 's/\t/\n/g' $i
sed -i '/^$/d' $i
sed -i '/hsa-mir-941-2/d' $i
sed -i '/hsa-mir-941-3/d' $i
sed -i '/hsa-mir-941-4/d' $i
sed -i '/hsa-mir-941-5/d' $i
sed -i '/hsa-mir-3180-2/d' $i
sed -i '/hsa-mir-3648-2/d' $i
sed -i '/hsa-mir-6511a-2/d' $i
done
