# Get 100,000 reads sample
# For PE, we need to keep same seed (-sXXXX) to get the same pairs
seqtk sample -s1234 CAPW8ANXX4AAGCTC_S1_L004_R1_001.fastq.gz 100000 > CAPW8ANXX4AAGCTC_S1_L004_R1_001_sample.fastq.gz
