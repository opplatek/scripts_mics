#!/bin/bash
#
# Scans nucleotide FASTA file and looks for non-IUPAC nucleotides https://www.biostars.org/p/162747/
# Optionally can replace all non-IUPAC nucleotides with Ns https://www.biostars.org/p/127714/
#

INPUT_FASTA=Burgud_pokus.fa

grep -v '^>' $INPUT_FASTA | sed 's/[a,A,g,G,c,C,t,T,u,U,n,N]//g'

sed -e '/^[^>]/s/[^ATGCatgc]/N/g' $INPUT_FASTA > ${INPUT_FASTA%.*}.IUPACclean.fa
