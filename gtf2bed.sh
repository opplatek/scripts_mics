#!/bin/bash
#
# Convert GTF to BED
#
# https://www.biostars.org/p/56280/
#

INPUT_GTF=Homo_sapiens.GRCh38.84.gtf


cat $INPUT_GTF |  awk 'OFS="\t" {if ($3=="gene") {print $1,$4-1,$5,$10,".",$7,$3}}' | tr -d '";' | sort -k1,1 -k2,2n > ${INPUT_GTF%.*}.bed
