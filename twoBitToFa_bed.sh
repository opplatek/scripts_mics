#!/bin/bash
#script for extracting fasta from 2bit format based on .bed coordinates
#please be careful about the genome version
#if you have newer/older genome than your .bed file please use liftOver utility

INTPUT_DIR=/home/jan/Data/projects/mirna_clusters/metadata/extract_clusters_10000/clusters_extra50;
OUTPUT_DIR=/home/jan/Data/projects/mirna_clusters/metadata/extract_clusters_10000/clusters_extra50;
PATH_TO_GENOME=/home/jan/Data/databases/human_genome/hg38.2bit;
BED_FILE=clusters_hsa_10000nt_final_extra50.bed

mkdir -p $OUTPUT_DIR;
cd $INTPUT_DIR;
for i in $BED_FILE;
do
	echo "Extracting" $i;
	/home/jan/Documents/scripts_commands/twoBitToFa -bed=$i $PATH_TO_GENOME $OUTPUT_DIR/${i%.*}.fasta;
done
