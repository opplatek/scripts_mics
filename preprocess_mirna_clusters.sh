#!/bin/bash

#preprocessing script designed for microRNA clusters project
#
#the script does following stepts in order to preprocess data
# for mapping
#1) adapter trimming
#2) quality trimming
#3) quality filtering
#4) size filtering

####################
##SPECIFY VARIABLES
DATASET=SRP012585 #dataset name

ADAPTER3_SEQ=TCGTATGCCGTCTTCTGCTTG #sequence of 3' adapter
             
ADAPTER5_SEQ=NA #sequence of 5' adapter

INPUT_SUFFIX=SRR493016*.fastq.gz #files suffix to launch the analysis on
PATH_TO_FILES=/home/jan/Projects/mirna_clusters/clusters_datasets/novel #path to datasets
OUTPUT_PATH=/home/jan/Projects/mirna_clusters/clusters_datasets/preprocessed/novel #specify output path
####################
FILE_FORMAT=fastq #file format
QUALITY=33 #phred coding of input files
##CUTADAPT##
ERROR_RATE=0.11 #allowed error rate or adapters
MIN_OVERLAP=6 #minimal overlap of adapters
DISC_SHORT=10 #discard length of sequences

##FASTX - QUALITY TRIMMING AND FILTERING
QT_THRESHOLD=20 #threshold for quality trimming
QF_THRESHOLD=15 #threshold for quality filtering
QF_PERC=85 #minimal percentage of bases with $ QF_THRESHOLD

##LENGTH FILTERING
MIN_LENGTH=14
MAX_LENGTH=30

####################
PATH_TO_FILES_LOAD=${PATH_TO_FILES}/${DATASET}
OUTPUT_PATH_SAVE=${OUTPUT_PATH}/${DATASET}
####################
ADAPTER3="${ADAPTER3_SEQ}AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" #stores 3' adapter
ADAPTER5=${ADAPTER5_SEQ} #stores 5' adapter
####################
cd $PATH_TO_FILES_LOAD;
####################
mkdir -p $OUTPUT_PATH_SAVE;
####################
##BODY OF THE SCRIPT
for sample in ${INPUT_SUFFIX};

do

cutadapt -f $FILE_FORMAT -a $ADAPTER3 -e $ERROR_RATE -O $MIN_OVERLAP -m $DISC_SHORT -o ${OUTPUT_PATH_SAVE}/${sample%.fastq*}.ad3trim.fq --info-file=${OUTPUT_PATH_SAVE}/${sample%.fastq*}.ad3info --too-short-output=${OUTPUT_PATH_SAVE}/${sample%.fastq*}.ad3short --untrimmed-output=${OUTPUT_PATH_SAVE}/${sample%.fastq*}.ad3untrimmed $sample;
#uncomment if 5'adapter is about to be trimmed
#cutadapt -f $FILE_FORMAT -g $ADAPTER5 -e $ERROR_RATE -O $MIN_OVERLAP -m $DISC_SHORT -o ${OUTPUT_PATH_SAVE}/${sample%.fastq*}.ad5trim.fq --info-file=${OUTPUT_PATH_SAVE}/${sample%.fastq*}.ad5info --too-short-output=${OUTPUT_PATH_SAVE}/${sample%.fastq*}.ad5short --untrimmed-output=${OUTPUT_PATH_SAVE}/${sample%.fastq*}.ad5untrimmed ${OUTPUT_PATH_SAVE}/${sample%.fastq*}.ad3trim.fq;

fastq_quality_trimmer -Q $QUALITY -t $QT_THRESHOLD -i ${OUTPUT_PATH_SAVE}/${sample%.fastq*}.ad3trim.fq -o ${OUTPUT_PATH_SAVE}/${sample%.fastq*}.qt.fq -l $DISC_SHORT;
#uncomment if 5'adapter is about to be trimmed and comment out the previous line
#fastq_quality_trimmer -Q $QUALITY -t $QT_THRESHOLD -i ${OUTPUT_PATH_SAVE}/${sample%.fastq*}.ad5trim.fq -o ${OUTPUT_PATH_SAVE}/${sample%.fastq*}.qt.fq -l $DISC_SHORT;

fastq_quality_filter -Q $QUALITY -q $QF_THRESHOLD -p $QF_PERC -i ${OUTPUT_PATH_SAVE}/${sample%.fastq*}.qt.fq -o ${OUTPUT_PATH_SAVE}/${sample%.fastq*}.qf.fq;

filter_by_length -o ${OUTPUT_PATH_SAVE}/${sample%.fastq*}.prep.fil.fq -e ${OUTPUT_PATH_SAVE}/${sample%.fastq*}.prep.filtout.fq -x $MAX_LENGTH -n $MIN_LENGTH ${OUTPUT_PATH_SAVE}/${sample%.fastq*}.qf.fq;

#if you need intermediate files comment next line. These files are good for analysis of trimming and improving the trimming and filtering
rm ${OUTPUT_PATH_SAVE}/${sample%.fastq*}.ad*trim.fq ${OUTPUT_PATH_SAVE}/${sample%.fastq*}.ad*info ${OUTPUT_PATH_SAVE}/${sample%.fastq*}.ad*short ${OUTPUT_PATH_SAVE}/${sample%.fastq*}.ad*untrimmed ${OUTPUT_PATH_SAVE}/${sample%.fastq*}.qt.fq ${OUTPUT_PATH_SAVE}/${sample%.fastq*}.qf.fq ${OUTPUT_PATH_SAVE}/${sample%.fastq*}.prep.filtout.fq;

done;
exit;


