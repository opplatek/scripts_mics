#!/bin/bash
#script for downloading the contents of starBase database
#as the input it takes a list of .bed from the starBase where
# each line is the name of file in starBase download directory

DOWNLOAD_DIRECTORY=/home/jan/Data/databases/starBase_v2
DOWNLOAD_LIST=/home/jan/Data/databases/starBase_v2/datasets.txt

while read list;
do
	echo "Downloading" $list;
	curl http://starbase.sysu.edu.cn/downloads/${list} > $DOWNLOAD_DIRECTORY/${list};
done < $DOWNLOAD_LIST;
