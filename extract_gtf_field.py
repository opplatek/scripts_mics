#!/usr/bin/env python
#
# Parse GTF file from GenBank GFF3 and extracts desired fields from the comment section
# To modify which comments you want alter the "print" line and add something you like
# Then replace "GTF_PATH_TO_REPLACE" with actuall path to the GTF and run as 
#	$ python extract_gtf_field.py
# 
# https://www.biostars.org/p/140471/; http://pygeno.iric.ca/

from pyGeno.tools.parsers.GTFTools import GTFFile

#gtfFilePath = "/home/jan/Data/projects/cincarova/2017/reference/MW2.gtf"
gtfFilePath = "GTF_PATH_TO_REPLACE"

gtf = GTFFile(gtfFilePath)
print "gene_id	gene_name	protein_id	gene_biotype	product"

for line in gtf :
   print line["gene_id"], "\t", line["gene_name"], "\t", line["protein_id"], "\t", line["gene_biotype"], "\t", line["product"]
