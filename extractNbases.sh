# Take only every second line and extract first 10, 11 or 12 bases, count number of occurences from the start of each read and sort
sed -n 2~4p tmp.fastq | cut -c-10 | sort | uniq | while read line;do echo -n $line; echo -n -e "\t"; grep ^$line tmp.fastq | wc -l;done | sort -k2,2nr > first10.txt &

sed -n 2~4p tmp.fastq | cut -c-12 | sort | uniq | while read line;do echo -n $line; echo -n -e "\t"; grep ^$line tmp.fastq | wc -l;done | sort -k2,2nr > first12.txt & 

sed -n 2~4p tmp.fastq | cut -c-11 | sort | uniq | while read line;do echo -n $line; echo -n -e "\t"; grep ^$line tmp.fastq | wc -l;done | sort -k2,2nr > first11.txt & 


