#!/bin/bash
#
# Finds indexed fastq reads and outputs them.
# If a read has index in first n bases (window_for_indexes) and last n bases it will read the fastq, grep all reads with both indexes and output them.
# MUST have fastq as input, doesn't work on fasta!!!
# Might be slow.
#
####################################################################################################
# Run as following
# $ chmod u+x separateIndexedReads_v2.sh
# $ ./separateIndexedReads_v2.sh input.fq window_for_indexes_left window_for_indexes_right index_left index_right
# Example
# $ ./separateIndexedReads_v2.sh test.fq 30 30 AAAAA TTTTT
####################################################################################################
### Variables - read from command line
INPUT=$1 # Input file

LEN_LEFT=$2 # Number of bases from the start where to look for Index 1
LEN_RIGHT=$3 # Number of bases from the end where to look for Index 2 - default is the same as for Index 1

INDEX1=$4 # Index 1 - from left
INDEX2=$5 # Index 2 - from right

####################################################################################################
## Check of input/output
if [[ -z $INPUT || -z $LEN_LEFT || -z $LEN_RIGHT || -z $INDEX1 || -z $INDEX2 ]]; then
	echo "One of the input variables is not set! Check if you have all in order - \"$ ./separateIndexedReads_v2.sh input.fq index_window_left index_window_right index1 index2\"."
	exit
fi

if [ ! -f $INPUT ]; then # Check input file presence
	echo "Input file" $INPUT "doesn't exist! Check and run again. Exiting..."
	exit
fi

if [ ! -s $INPUT ]; then # Check input file size
	echo "Input file" $INPUT "is empty! Check and run again. Exiting..."
	exit
fi

if [ -f ${INPUT%.*}.$INDEX1.$INDEX2.fq ]; then # If output file already exists give warning and exit - this is because we append the output to one file
    echo -e "Output file" ${INPUT%.*}.$INDEX1.$INDEX2.fq "already exists! Is this on purpose?\n"
	read -n1 -r -p "Press 'y' to continue...else abort the mission!" key
	if [ "$key" = 'y' ]; then
		echo -e "\n 'y' pressed, continue the analysis!"
	else
		echo -e "\n 'y' NOT pressed, abort the analysis!"
		exit
	fi
fi 

echo "Processing input file" $INPUT "with indexes" $INDEX1 "for left index and" $INDEX2 "for right index in window" $LEN_LEFT". Will output file" ${INPUT%.*}.$INDEX1.$INDEX2.fq

####################################################################################################
### Script body
a=$RANDOM # Generate random number for tmp results and not to ovewrite other results

counter=0 # Counter to help
touch ${a}lines1 # Pre-create temporary files to avoid empty file
touch ${a}lines2
touch ${INPUT%.*}.$INDEX1.$INDEX2.fq

awk 'NR%4==2' $INPUT | while read line # Read file line by line stripping .fastq formating keeping only sequence (read every fourth line starting on line 2)
do 
	counter=$[$counter+1] # Move counter by one = next line

	LEN=${#line} # Get line length

	BACK=$[$LEN-$LEN_RIGHT] # Store back of the sequence where to look for Index 2

	if echo $line | cut -c 1-$LEN_LEFT  | grep $INDEX1; then # Scan for index 1
		echo "Index 1 found in read" $counter
		echo $counter >> ${a}lines1 # If found, save line number
	else
		echo "Index 1 not found in read" $counter
	fi

	if echo $line | cut -c $BACK-$LEN | grep $INDEX2; then # Scan for index 2
		echo "Index 2 found in read" $counter
		echo $counter >> ${a}lines2  # If found, save line number
	else
		echo "Index 2 not found in read" $counter
	fi

done

cat ${a}lines1 ${a}lines2 | sort | uniq -d | while read line # Read lines numbers one by one but only when both indexes are present!
do 

	line_tmp=$[$line-1]

	start=$[$line_tmp*4+1] # Store fastq read - start
	end=$[$line*4] # Store fastq read - end 

	sed -n ${start},${end}p $INPUT >> ${INPUT%.*}.$INDEX1.$INDEX2.fq # write result to output
done

rm ${a}lines1 ${a}lines2 # Clean

if [ -s ${INPUT%.*}.$INDEX1.$INDEX2.fq ]; then
	echo "Some reads with both adapters were found, hurray! Check the result at" ${INPUT%.*}.$INDEX1.$INDEX2.fq
else
	echo "Output file" ${INPUT%.*}.$INDEX1.$INDEX2.fq "looks empty, probably no reads with adapters were found...?"
fi
