#!/bin/bash
#script for downloading sra files from SRA NCBI database
#as input it takes file with list of datasets to download
#this input file has to be in this format
#SRPname_of_the_experiment SRRname_of_the_dataset
#where the blank space is normal space
#if you have this input file ready you can launch the script as following
#while read list;do ./download_sra_ncbi.sh $list;done < list_ncbi.txt
#"list_ncbi.txt" is the list with experiment names and dataset names
#this script then takes the name of the experiment as the folder where it should 
#save all dataset from this experiment, creates the folder according to the name 
#of the experiment and saves .sra files of the datasets

mkdir $1
curl ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByRun/sra/${2:0:3}/${2:0:6}/${2}/${2}.sra > ${1}/${2}.sra






