#!/bin/bash
#
# Sort VCF by chromosome names
# https://www.biostars.org/p/133487/
#

IN=in.vcf

grep '^#' $IN > ${IN%.vcf}.sorted.vcf && grep -v '^#' $IN | LC_ALL=C sort -t $'\t' -k1,1 -k2,2n >> ${IN%.vcf}.sorted.vcf

