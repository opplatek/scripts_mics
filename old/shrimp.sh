VERSION="revisited"
#specify output directory
OUTPUT_DIR=/home/jan/Projects/mirna_clusters/calculation/shrimp_out/${VERSION}
#make output directory
mkdir -p $OUTPUT_DIR
#specify the reference
REF_GEN=/home/jan/Data/databases/miRBase_v21/hsa_stemloop.fa
REF_GEN2=${SHRIMP_FOLDER}/database/"$(basename "$REF_GEN" .fa)-ls"
REF_GTF=/home/jan/Data/databases/miRBase_v21/hsa_stemloop.gff2
#################################################################################################
#to create reference for miRNA stemloops for HTSeq from miRBase you have to 
#1) change "ID=" to "gene_id " (including the space)
#2) change all "-" from the strand info to "+" (might not be necessary)
#3) create new coordinates in a following manner - set "start" to "1"
#                                                  set "end" to "original end-original start+1"
#4) Remove the header (usually starts with #) - HTSeq will recognize the gtf file but will report
#   everything as no_feature subjects
#################################################################################################
#project the database/reference index
#NOTE this is designed for Illumina MATURE SEQUENCE mapping - each string of 1s is representing continuous string of accurate mappings - not mismatches
#NOTE run this only once
#$SHRIMP_FOLDER/utils/project-db.py --seed 01111111001111111100,01111111110011111100,01111111111100111100,01111111111111001100,01111111111111110000 --h-flag --shrimp-mode ls --dest-dir=/home/jan/Tools/SHRiMP_2_2_3/database $REF_GEN
###
#NOTE this is designed for Illumina STEM-LOOP SEQUENCE mapping
#$SHRIMP_FOLDER/utils/project-db.py --h-flag --shrimp-mode ls --dest-dir=/home/jan/Tools/SHRiMP_2_2_3/database $REF_GEN

#make the output directory
OUTPUT_DIR2=${PWD##*/}
mkdir -p $OUTPUT_DIR/${OUTPUT_DIR2}

#launch the mapping
for i in *.prep.fil.fq
do
#removed -U #worked
#removed -a 0
#removed -g -255
#removed -q -255
#
#gmapper-ls -L /home/jan/Tools/SHRiMP_2_2_3/database/stemloop-ls $i -H --strata --open-r -255 -q -255 -n 1 -w 100% -a 0 --qv-offset 33 --no-mapping-qualities -U -Z -Q -N 6 >$OUTPUT_DIR/${OUTPUT_DIR2}/${i%.*}${VERSION}.out 2>$OUTPUT_DIR/${OUTPUT_DIR2}/${i%.*}${VERSION}.log
#NOTE original -M mirna setting "-H -n 1 -w 100% -U -a 0 -g -255 -q -255 -Z" which DOES NOT work
gmapper-ls -L $REF_GEN2 $i -M mirna --qv-offset 33 -Q -N 3 --strata --min-avg-qv 17 --sam --sam-unaligned > $OUTPUT_DIR/${OUTPUT_DIR2}/${i%.*}.sam 2>$OUTPUT_DIR/${OUTPUT_DIR2}/${i%.*}.log
#samtools processing and sorting
#extract only aligned reads and convert to bam
#samtools view -Sb -F 4 $OUTPUT_DIR/${OUTPUT_DIR2}/${i%.*}${VERSION}.sam > $OUTPUT_DIR/${OUTPUT_DIR2}/${i%.*}${VERSION}.bam
#convert to bam all reads
samtools view -Sb $OUTPUT_DIR/${OUTPUT_DIR2}/${i%.*}.sam > $OUTPUT_DIR/${OUTPUT_DIR2}/${i%.*}.bam
#make statistics
samtools flagstat $OUTPUT_DIR/${OUTPUT_DIR2}/${i%.*}.bam > $OUTPUT_DIR/${OUTPUT_DIR2}/${i%.*}.flagstat
#sort bam file by coordinates
java -Xmx2G -jar ~/Tools/picard-tools-1.108/SortSam.jar SORT_ORDER=coordinate INPUT=$OUTPUT_DIR/${OUTPUT_DIR2}/${i%.*}.bam OUTPUT=$OUTPUT_DIR/${OUTPUT_DIR2}/${i%.*}.sorted.bam
#remove original sam file
rm $OUTPUT_DIR/${OUTPUT_DIR2}/${i%.*}.sam
#remove unsorted bam file keeping only coordinate sorted bam
rm $OUTPUT_DIR/${OUTPUT_DIR2}/${i%.*}.bam
#launch NGSutils and filter out reads that are unmapped, fail QC and have more than 1 mismatches
~/Tools/ngsutils/bin/bamutils filter $OUTPUT_DIR/${OUTPUT_DIR2}/${i%.*}.sorted.bam $OUTPUT_DIR/${OUTPUT_DIR2}/${i%.*}.filtered.bam -mapped -mismatch 1 -noqcfail > $OUTPUT_DIR/${OUTPUT_DIR2}/${i%.*}.filter.log
#count features using HTSeq-count
######################################################replace miRNA for tRNA or whatever the second/third column is
htseq-count -f bam -r pos -s no -t miRNA $OUTPUT_DIR/${OUTPUT_DIR2}/${i%.*}.filtered.bam $REF_GTF > $OUTPUT_DIR/${OUTPUT_DIR2}/${i%.*}.filtered.counts
done 

