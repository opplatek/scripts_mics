#
# Make gff from blast results This is designed for making gff annotation from mature mirna -> stemloop mirna mapping bp_search2gff.pl
makeblastdb -in hsa_stemloop.fa -dbtype nucl
#https://www.biostars.org/p/120580/#120586; http://www.ncbi.nlm.nih.gov/books/NBK279675/
blastn -query hsa_mature.fa -db hsa_stemloop.fa -task blastn-short -qcov_hsp_perc 100 -perc_identity 100 -evalue 100 -strand plus > blast.txt
# https://www.biostars.org/p/277/
bp_search2gff.pl --input blast.txt --addid --version 3 --type hit > hsa_mature.gff
