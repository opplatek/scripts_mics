#!/bin/bash

#script to extract biotype types from GTF
#launch it as following awk -f get.gtf.ensg.biotypes.awk Homo_sapiens.GRCh37.71.gtf

BEGIN {OFS=FS="\t"}
(substr($1,1,1)!="#" && substr($1,2,1)!="#") {
        split($9,format,";");
        i=0;
           for (i in format){
                if (format[i] ~ /gene_biotype|gene_type/){
                  gsub("gene_biotype | gene_type ", "", format[i]);
                  gsub(/"/,"",format[i]);
                  gsub(/gene_id "/,"",format[1]);
                  gsub(/transcript_id "/,"",format[2]);
                  gsub(/"/,"",format[1]);
                  gsub(/"/,"",format[2]);
                        #print format[1] "\t" format[i];
                        print format[1] "\t" format[2] "\t" format[i];
                }
            }
        }
