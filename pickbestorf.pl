#!/usr/bin/perl

#pickbestorf: picks best match for each query
#or pick best match for each swissprot protein (change: $id = $1; to -> $id = $2;)
#sort for id-s (or orfs) beforehand
				
while (<>) {			
    ($id,$orf,$rest) = split;
    #is current line beginning of a new record?
    if ($id ne $lastid) {
	#is there an old record to handle?
	if ($rec) {		# 
	    finish_record($rec);
	}
	#initialize new record:
	$rec=$_;
    } else {
	$rec=$rec.$_;
    }
    $lastid=$id;
}
#is there an old record to handle?
if ($rec) {
    finish_record($rec);
}
exit;
############################################################################
#handle the matches between a query and an orf, 
#select one orf for each query with best e-value
sub finish_record {
    my ($record) = @_;
    my $evalmin=100;
    my $selectline="";
    @recordlist=split("\n",$record);
    while (@recordlist) {
	$line=shift(@recordlist);
	my ($id,$orf,$start,$stop,$idstart,$idstop,$eval,$rest) = split("\t",$line);
	if ($eval<$evalmin) {
	    $selectline=$line;
	    $evalmin=$eval;#was missing!!! have to insert it into select-uniq-query-orf-combinations also!!!
	}
    }
    print "$selectline\n";
}
#subroutine the same as for select-uniq-query-orf-combinations
